//
//  AddDataViewController.m
//  Contacts
//
//  Created by Stuart Adams on 3/23/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "AddDataViewController.h"

@implementation AddDataViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    int lblWidth = 90;
    int lblHeight = 15;
    int padding = 10;
    int textFieldWidth = 200;
    int textFieldHeight = 20;
    
    UIImageView* backgroundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"paper"]];
    backgroundImage.frame = self.view.bounds;
    
    [self.view addSubview:backgroundImage];
    
    firstNameLbl = [self makeLbl:firstNameLbl
                          X:10
                          Y:60
                      width:lblWidth
                     height:lblHeight
                  labelText:@"First Name"
                       view:self.view];
    
    txtFirstName = [self makeTextField:txtFirstName
                                     x:firstNameLbl.frame.origin.x + firstNameLbl.frame.size.width+ padding
                                     y:firstNameLbl.frame.origin.y
                                 width:textFieldWidth
                                height:textFieldHeight];
    
    lastNameLbl = [self makeLbl:lastNameLbl
                               X:firstNameLbl.frame.origin.x
                               Y:firstNameLbl.frame.origin.y + firstNameLbl.frame.size.height + padding
                           width:lblWidth
                          height:lblHeight
                       labelText:@"Last Name"
                            view:self.view];
    
    txtLastName = [self makeTextField:txtLastName
                                     x:txtFirstName.frame.origin.x
                                     y:lastNameLbl.frame.origin.y
                                 width:textFieldWidth
                                height:textFieldHeight];
    
    phoneNumberLbl = [self makeLbl:phoneNumberLbl
                              X:lastNameLbl.frame.origin.x
                              Y:lastNameLbl.frame.origin.y + lastNameLbl.frame.size.height + padding
                          width:lblWidth
                         height:lblHeight
                      labelText:@"Phone"
                           view:self.view];
    
    txtPhone = [self makeTextField:txtPhone
                                    x:txtFirstName.frame.origin.x
                                    y:phoneNumberLbl.frame.origin.y
                                width:textFieldWidth
                               height:textFieldHeight];
    
    addressLbl = [self makeLbl:addressLbl
                                 X:phoneNumberLbl.frame.origin.x
                                 Y:phoneNumberLbl.frame.origin.y + phoneNumberLbl.frame.size.height + padding
                             width:lblWidth
                            height:lblHeight
                         labelText:@"Address"
                              view:self.view];
    
    txtAddress = [self makeTextField:txtAddress
                                    x:txtFirstName.frame.origin.x
                                    y:addressLbl.frame.origin.y
                                width:textFieldWidth
                               height:textFieldHeight];
    
    emailLbl = [self makeLbl:emailLbl
                             X:addressLbl.frame.origin.x
                             Y:addressLbl.frame.origin.y + addressLbl.frame.size.height + padding
                         width:lblWidth
                        height:lblHeight
                     labelText:@"Email"
                          view:self.view];
    
    txtEmail = [self makeTextField:txtEmail
                                    x:txtFirstName.frame.origin.x
                                    y:emailLbl.frame.origin.y
                                width:textFieldWidth
                               height:textFieldHeight];
    
    facebookLinkLbl = [self makeLbl:facebookLinkLbl
                           X:emailLbl.frame.origin.x
                           Y:emailLbl.frame.origin.y + emailLbl.frame.size.height + padding
                       width:lblWidth
                      height:lblHeight
                   labelText:@"Facebook"
                        view:self.view];
    
    txtFacebookLink = [self makeTextField:txtFacebookLink
                                    x:txtFirstName.frame.origin.x
                                    y:facebookLinkLbl.frame.origin.y
                                width:textFieldWidth
                               height:textFieldHeight];
    
    twitterLinkLbl = [self makeLbl:twitterLinkLbl
                                 X:facebookLinkLbl.frame.origin.x
                                 Y:facebookLinkLbl.frame.origin.y + facebookLinkLbl.frame.size.height + padding
                             width:lblWidth
                            height:lblHeight
                         labelText:@"Twitter"
                              view:self.view];
    
    txtTwitterLink = [self makeTextField:txtTwitterLink
                                    x:txtFirstName.frame.origin.x
                                    y:twitterLinkLbl.frame.origin.y
                                width:textFieldWidth
                               height:textFieldHeight
                      returnKeyType:UIReturnKeyDone];
    
    UIButton* btnSave = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnSave setTitle:@"Save" forState:UIControlStateNormal];
    [btnSave addTarget:self action:@selector(saveData:) forControlEvents:UIControlEventTouchUpInside];
    btnSave.frame = CGRectMake(self.view.frame.size.width/2 - 25, twitterLinkLbl.frame.origin.y + twitterLinkLbl.frame.size.height + padding, 50, 25);
    [self.view addSubview:btnSave];
}

-(void) saveData:(id)sender
{
    NSManagedObjectContext* context = self.managedObjectContext;
    self.contactItem = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"ContactInfo"
                                    inManagedObjectContext:context];
    
    self.contactItem.first_name = txtFirstName.text;
    self.contactItem.last_name = txtLastName.text;
    self.contactItem.phone_number = txtPhone.text;
    self.contactItem.address = txtAddress.text;
    self.contactItem.facebookLink = txtFacebookLink.text;
    self.contactItem.email = txtEmail.text;
    self.contactItem.twitterLink = @"twitter";
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
//    [contactInfo setValue:txtFirstName.text forKey:@"first_name"];
//    [contactInfo setValue:txtLastName.text forKey:@"last_name"];
//    [contactInfo setValue:txtPhone.text forKey:@"phone_number"];
//    [contactInfo setValue:txtEmail.text forKey:@"email"];
//    [contactInfo setValue:txtFacebookLink.text forKey:@"facebookLink"];
//    [contactInfo setValue:txtTwitterLink.text forKey:@"twitterLink"];
//    [contactInfo setValue:txtAddress.text forKey:@"address"];
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(UITextField*)makeTextField:(UITextField*)textField
                           x:(int)x
                           y:(int)y
                       width:(int)width
                      height:(int)height
               returnKeyType:(int)returnKeyType
{
    textField = [[UITextField alloc]initWithFrame:CGRectMake(x, y, width, height)];
    textField.layer.borderColor = [[UIColor lightCharcoalColor] CGColor];
    textField.backgroundColor = [UIColor offWhiteColor];
    textField.delegate = self;
    textField.text = @"";
    textField.returnKeyType = returnKeyType;
    textField.adjustsFontSizeToFitWidth = TRUE;
    textField.minimumFontSize = 10;

    [self.view addSubview:textField];
    return textField;
}

-(UITextField*)makeTextField:(UITextField*)textField
                           x:(int)x
                           y:(int)y
                       width:(int)width
                      height:(int)height
{
    textField = [[UITextField alloc]initWithFrame:CGRectMake(x, y, width, height)];
    textField.layer.borderColor = [[UIColor lightCharcoalColor] CGColor];
    textField.backgroundColor = [UIColor offWhiteColor];
    textField.delegate = self;
    textField.returnKeyType = UIReturnKeyNext;
    textField.text = @"";
    [textField canBecomeFirstResponder];
    textField.adjustsFontSizeToFitWidth = TRUE;
    textField.minimumFontSize = 10;
    [self.view addSubview:textField];
    return textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtFirstName)
    {
        [txtLastName becomeFirstResponder];
    }
    else if(textField == txtLastName)
    {
        [txtPhone becomeFirstResponder];
    }
    else if (textField == txtPhone)
    {
        [txtAddress becomeFirstResponder];
    }
    else if (textField == txtAddress)
    {
        [txtEmail becomeFirstResponder];
    }
    else if(textField == txtEmail)
    {
        [txtFacebookLink becomeFirstResponder];
    }
    else if (textField == txtFacebookLink)
    {
        [txtTwitterLink becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

-(UILabel*)makeLbl:(UILabel*)label
                 X:(int)x
                 Y:(int)y
             width:(int)width
            height:(int)height
         labelText:(NSString*)text
              view:(id)viewIn
{
    label = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    label.text = text;
    [viewIn addSubview:label];
    return label;
}



@end
