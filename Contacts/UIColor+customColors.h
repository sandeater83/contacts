//
//  UIColor+customColors.h
//  Contacts
//
//  Created by Stuart Adams on 3/24/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+(UIColor*)lightCharcoalColor;
+(UIColor*)offWhiteColor;

@end
