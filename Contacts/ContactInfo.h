//
//  ContactInfo.h
//  Contacts
//
//  Created by Stuart Adams on 3/24/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ContactInfo : NSManagedObject

@property (nonatomic, retain) NSData * contactImage;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * facebookLink;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * phone_number;
@property (nonatomic, retain) NSString * twitterLink;
@property (nonatomic, retain) NSString * address;

@end
