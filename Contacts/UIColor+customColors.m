//
//  UIColor+customColors.m
//  Contacts
//
//  Created by Stuart Adams on 3/24/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

/*********************
 
 COLORS
 ---------------------
 Light Charcoal       #595059
 Off White            #ede9e1
 *********************/

#import "UIColor+customColors.h"

@implementation UIColor (CustomColors)
+(UIColor*)lightCharcoalColor
{
    return [UIColor colorWithRed:0.349 green:0.314 blue:0.349 alpha:1]; /*#595059*/
}

+(UIColor*)offWhiteColor
{
    return [UIColor colorWithRed:0.929 green:0.914 blue:0.882 alpha:1]; /*#ede9e1*/
}

@end
