//
//  DetailViewController.m
//  Contacts
//
//  Created by Stuart Adams on 3/19/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

//- (void)setDetailItem:(id)newDetailItem {
//    if (_detailItem != newDetailItem) {
//        _detailItem = newDetailItem;
//    }
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    int padding = 20;
    int stdLblHeight = 35;
    int stdLblWidth = 200;
    int phoneWidth = 150;
    int squareBtnDimension = 50;
    
    NSLog(@"%@", self.detailItem);
    
    NSString* fullName = [NSString stringWithFormat:@"%@ %@", [[self.detailItem valueForKey:@"first_name"] description], [[self.detailItem valueForKey:@"last_name"] description]];
    
    self.title = fullName;
    
    UIImageView* backGround = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"paper"]];
    backGround.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:backGround];
    name = [[UILabel alloc]initWithFrame:CGRectMake(10, 70, stdLblWidth, stdLblHeight)];
    name.text = fullName;
    //UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showAddress:)];
//    [firstName addGestureRecognizer:tapGesture];
    
    address = [[UILabel alloc]initWithFrame:CGRectMake(name.frame.origin.x, name.frame.origin.y + name.frame.size.height + padding, stdLblWidth, stdLblHeight * 2)];
    address.text = [[self.detailItem valueForKey:@"address"] description];
    address.numberOfLines = 0;
    address.lineBreakMode = NSLineBreakByWordWrapping;
    
    phoneNumber = [[UILabel alloc]initWithFrame:CGRectMake(address.frame.origin.x, address.frame.origin.y + address.frame.size.height + padding, phoneWidth, stdLblHeight)];
    phoneNumber.text = [[self.detailItem valueForKey:@"phone_number"] description];
    
    email = [[UILabel alloc]initWithFrame:CGRectMake(phoneNumber.frame.origin.x, phoneNumber.frame.origin.y + phoneNumber.frame.size.height + padding, stdLblWidth, stdLblHeight)];
    email.text = [[self.detailItem valueForKey:@"email"] description];
    
    facebookLink = [[UILabel alloc]initWithFrame:CGRectMake(email.frame.origin.x, email.frame.origin.y + email.frame.size.height + padding, stdLblWidth, stdLblHeight)];
    facebookLink.text = [[self.detailItem valueForKey:@"facebookLink"] description];
    
    twitterLink = [[UILabel alloc]initWithFrame:CGRectMake(facebookLink.frame.origin.x, facebookLink.frame.origin.y + facebookLink.frame.size.height + padding, stdLblWidth, stdLblHeight)];
    twitterLink.text = [[self.detailItem valueForKey:@"twitterLink"] description];
    
//    imgFaceBookIcon = [UIImage imageNamed:@"facebook"];
//    btnCall = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [btnCall setBackgroundImage:imgFaceBookIcon forState:UIControlStateNormal];
//    btnCall.frame = CGRectMake(phoneNumber.frame.origin.x + phoneNumber.frame.size.width + padding, phoneNumber.frame.origin.y, squareBtnDimension, squareBtnDimension);
//    [btnCall addTarget:self action:@selector(makeCall:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnMap = [self makeImageButton:btnMap
                         ImageName:@"direction"
                                 X:address.frame.origin.x + address.frame.size.width + padding
                                 Y:address.frame.origin.y
                             width:squareBtnDimension
                            height:squareBtnDimension
                         addToView:self.view
                      targetAction:@selector(showAddress:)];
    
    btnText = [self makeImageButton:btnText
                          ImageName:@"text"
                                  X:btnMap.frame.origin.x
                                  Y:phoneNumber.frame.origin.y
                              width:squareBtnDimension
                             height:squareBtnDimension
                          addToView:self.view
                       targetAction:@selector(sendText:)];
    
    btnCall = [self makeImageButton:btnCall
                          ImageName:@"phone"
                                  X:btnText.frame.origin.x - btnText.frame.size.width - padding
                                  Y:phoneNumber.frame.origin.y
                              width:squareBtnDimension
                             height:squareBtnDimension
                                 addToView:self.view
                                targetAction:@selector(makeCall:)];
    
    btnEmail = [self makeImageButton:btnEmail
                           ImageName:@"email"
                                   X: email.frame.origin.x + email.frame.size.width + padding
                                   Y:email.frame.origin.y
                               width:squareBtnDimension
                              height:squareBtnDimension
                           addToView:self.view
                        targetAction:@selector(sendEmail:)];
    
    btnFacebook = [self makeImageButton:btnFacebook
                              ImageName:@"facebook"
                                      X:facebookLink.frame.origin.x + facebookLink.frame.size.width + padding
                                      Y:facebookLink.frame.origin.y
                                  width:squareBtnDimension
                                 height:squareBtnDimension
                              addToView:self.view
                           targetAction:@selector(visitFacebook:)];
    
    btnTwitter = [self makeImageButton:btnTwitter
                              ImageName:@"twitter"
                                      X:twitterLink.frame.origin.x + twitterLink.frame.size.width + padding
                                      Y:twitterLink.frame.origin.y
                                  width:squareBtnDimension
                                 height:squareBtnDimension
                              addToView:self.view
                           targetAction:@selector(visitTwitter:)];
    NSLog(@"%@",phoneNumber.text);
    NSLog(@"%@", twitterLink.text);
    
    [self.view addSubview:name];
    [self.view addSubview:address];
    [self.view addSubview:phoneNumber];
    [self.view addSubview:email];
    [self.view addSubview:facebookLink];
    [self.view addSubview:twitterLink];
    
    if([address.text  isEqual: @""] || [[self.detailItem valueForKey:@"address"] description] == nil)
    {
        btnMap.hidden = TRUE;
    }
    if([phoneNumber.text  isEqual: @""] || [[self.detailItem valueForKey:@"phone_number"] description] == nil)
    {
        btnCall.hidden = TRUE;
        btnText.hidden = TRUE;
    }
    if([email.text  isEqual: @""] || [[self.detailItem valueForKey:@"email"] description] == nil)
    {
        btnEmail.hidden = TRUE;
    }
    if([facebookLink.text  isEqual: @""] || [[self.detailItem valueForKey:@"facebookLink"] description] == nil)
    {
        btnFacebook.hidden = TRUE;
    }
    if([twitterLink.text  isEqual: @""] || [[self.detailItem valueForKey:@"twitterLink"] description] == nil)
    {
        btnTwitter.hidden = TRUE;
    }
}

-(UIButton*)makeImageButton:(UIButton*)button
                  ImageName:(NSString*)imgName
                          X:(int)x
                          Y:(int)y
                      width:(int)width
                     height:(int)height
                  addToView:(id)view
               targetAction:(SEL)selector
{
    UIImage* btnImage = [UIImage imageNamed:imgName];
    button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setBackgroundImage:btnImage forState:UIControlStateNormal];
    button.frame = CGRectMake(x, y, width, height);
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    return button;
}

-(void)showAddress:(id)sender
{
    if(self.geocoder == nil)
    {
        self.geocoder = [CLGeocoder new];
    }
    
    [self.geocoder geocodeAddressString:address.text completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count > 0) {
            CLPlacemark* placemark = [placemarks objectAtIndex:0];
            CLLocation* location = placemark.location;
            float latitude = location.coordinate.latitude;
            float longitude = location.coordinate.longitude;
            NSString* url = [NSString stringWithFormat:@"http://maps.apple.com/maps?q=%f,%f",
                             latitude,
                             longitude];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
        }else if(error.domain == kCLErrorDomain)
        {
            switch (error.code) {
                case kCLErrorDenied:
                    NSLog(@"Location Services Denied by User");
                    break;
                case kCLErrorNetwork:
                    NSLog(@"No Network");
                    break;
                    
                case kCLErrorGeocodeFoundNoResult:
                    NSLog(@"No Results found");
                    break;
                    
                default:
                    NSLog(error.localizedDescription);
                    break;
            }
        }
        else{
            NSLog(error.localizedDescription);
        }

    }];
}

-(void)makeCall:(id)sender
{
    //Checking to see if the devices is a phone
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        //If phone use the phone application passing in the contact's phone number
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber.text]];
    } else {
        //Otherwise notify user that this feature is not available on their device via aleart box
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
    
}

-(void)sendText:(id)sender
{
    MFMessageComposeViewController* smsController = [MFMessageComposeViewController new];
    smsController.messageComposeDelegate = self;
    if(smsController)
    {
        [self presentViewController:smsController animated:YES completion:^{
            
        }];
    }
    
}

-(void)sendEmail:(id)sender
{
    MFMailComposeViewController* controller = [MFMailComposeViewController new];
    
    controller.mailComposeDelegate = self;
    
    [controller setToRecipients:@[email.text]];
    
    if(controller)
    {
        [self presentViewController:controller animated:YES completion:^{
            
        }];
    }
}

-(void)visitFacebook:(id)sender
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:facebookLink.text]];
}

-(void)visitTwitter:(id)sender
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:twitterLink.text]];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
