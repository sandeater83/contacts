//
//  DetailViewController.h
//  Contacts
//
//  Created by Stuart Adams on 3/19/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "ContactInfo.h"

@interface DetailViewController : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate>
{
    UILabel* name;
    UILabel* address;
    UILabel* phoneNumber;
    UILabel* email;
    UILabel* facebookLink;
    UILabel* twitterLink;
    UIButton* btnCall;
    UIButton* btnText;
    UIButton* btnEmail;
    UIButton* btnMap;
    UIButton* btnFacebook;
    UIButton* btnTwitter;
    UIImage* imgContactPic;
    MKMapView* map;
    CLLocationManager* locationManager;
}

@property (strong, nonatomic) ContactInfo* contactDetails;
@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property(strong, nonatomic) CLGeocoder* geocoder;

@end

