//
//  AddDataViewController.h
//  Contacts
//
//  Created by Stuart Adams on 3/23/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "UIColor+customColors.h"
#import "ContactInfo.h"

@interface AddDataViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate>
{
    UILabel* firstNameLbl;
    UILabel* lastNameLbl;
    UILabel* phoneNumberLbl;
    UILabel* twitterLinkLbl;
    UILabel* facebookLinkLbl;
    UILabel* addressLbl;
    UILabel* emailLbl;
    UITextField* txtFirstName;
    UITextField* txtLastName;
    UITextField* txtAddress;
    UITextField* txtPhone;
    UITextField* txtFacebookLink;
    UITextField* txtTwitterLink;
    UITextField* txtEmail;
}

@property(strong, nonatomic) ContactInfo* contactItem;

@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@end
