//
//  ContactInfo.m
//  Contacts
//
//  Created by Stuart Adams on 3/24/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ContactInfo.h"


@implementation ContactInfo

@dynamic contactImage;
@dynamic email;
@dynamic facebookLink;
@dynamic first_name;
@dynamic last_name;
@dynamic phone_number;
@dynamic twitterLink;
@dynamic address;

@end
